#!/bin/bash

export FLASK_APP=app.py
export FLASK_DEBUG=1
export PBP_SETTINGS=config/settings-dev.cfg
flask run
