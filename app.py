from flask import Flask, Response, redirect, render_template, request

from flask_login import (LoginManager, current_user, login_required,
                         logout_user)
from repository.budgetrepository import BudgetRepository
from repository.userrepository import UserRepository
from service.userservice import UserService
from utility import formhelpers


app = Flask(__name__)
app.config.from_envvar('PBP_SETTINGS')

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

budget_repository = BudgetRepository()

user_repository = UserRepository()
user_service = UserService()


@app.route("/")
@login_required
def index():
    return render_template('index.html')


@app.route("/login")
def login():
    return render_template("login.html")


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect('/login')


@app.route("/authenticate", methods=["POST"])
@formhelpers.validate_login_json
def authenticate():
    return user_service.authenticate_user(request.json)


@app.route("/getBudgetData")
@login_required
def get_budget_data():
    return Response(budget_repository.read_budget_data(current_user),
                    mimetype='application/json')


@app.route("/saveBudgetData", methods=['POST'])
@login_required
def save_budget_data():
    if request.json:
        budget_repository.write_budget_data(current_user, request.json)
    return Response(budget_repository.read_budget_data(current_user),
                    mimetype='application/json')


@app.route("/modifyUser", methods=['POST'])
@login_required
@formhelpers.validate_modification_json
def modify_user():
    return user_service.modify_user(request.json, current_user.id)


@app.route("/addUser", methods=['POST'])
@formhelpers.validate_registration_json
def add_user():
    return user_service.add_user(request.json)


@login_manager.user_loader
def load_user(id):
    return user_repository.get_user(id)


if __name__ == "__main__":
    app.run()
