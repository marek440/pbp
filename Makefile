
install:
	virtualenv venv
	venv/bin/pip install -r requirements.txt;

clean:
	find . -type f -name '*.pyc' -exec rm {} +
	rm -rf venv

test:
	python -m tests.AppTestCase -v

run:
	./startdebug.sh

runpublic:
	./startdebug_public.sh
