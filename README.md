# Personal Budget Planner #

Personal Budget Planner (PBP) is a software meant for helping individuals to track their personal cash flow.

## Getting Started ##
The following instructions will help you to set your copy of the project source code up and running at your local PC.

## Prerequisites ##

The following software is needed prior setup of the project:

* git client
* make utility
* Python interpreter
* Python virtualenv package

## Installing and Running ##

Instructions for setting up the development environment and starting up the application:

### Environment setup ###

```
#!bash

$ git clone https://bitbucket.org/marek440/pbp.git
$ cd pbp
$ make install
```

### Activating the virtual environment ###

```
#!bash

$ source venv/bin/activate
```

### Running the tests ###

```
#!bash

$ make test
```

### Running the application ###

```
#!bash

$ make run
```

Now you should be able to see the application running at [http://localhost:5000](http://localhost:5000)

### Stopping the application: ###

```
#!bash

<CTRL> + C
```

### Deactivating and cleaning up the virtual environment ###

```
#!bash

$ deactivate
$ make clean
```

## Development ##

This is an open source project. Contributions are welcome in any form.

Bugs & Feature Requests can be tracked and inserted [here](https://bitbucket.org/marek440/pbp/issues/).

## Authors ##

* **Marek Lints** (marek.lints [AT] gmail.com) - *Initial work* - [marek440](https://bitbucket.org/marek440/)

## License ##

### The MIT License (MIT) ###

*Copyright (c) 2017 Marek Lints (marek.lints [AT] gmail.com)*

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.