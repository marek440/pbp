from functools import wraps

from flask import jsonify, request

from jsonschema import FormatChecker
from jsonschema.exceptions import ValidationError
from jsonschema.validators import validate


MODIFICATION_SCHEMA = {
    "type": "object",
    "properties": {
        "password": {"type": "string", "minLength": 5, "maxLength": 50},
        "password2": {"type": "string", "minLength": 5, "maxLength": 50}
    },
    "required": ["password", "password2"]
}


REGISTRATION_SCHEMA = {
    "type": "object",
    "properties": {
        "id": {"type": "string", "format": "email"},
        "password": {"type": "string", "minLength": 5, "maxLength": 50},
        "password2": {"type": "string", "minLength": 5, "maxLength": 50}
    },
    "required": ["id", "password", "password2"]
}


LOGIN_SCHEMA = {
    "type": "object",
    "properties": {
        "id": {"type": "string", "minLength": 5},
        "password": {"type": "string", "minLength": 5, "maxLength": 50},
    },
    "required": ["id", "password"]
}


def validate_modification_json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            validate(request.json, MODIFICATION_SCHEMA)
            if request.json["password"] != request.json["password2"]:
                return jsonify({'success': False,
                                'showError': True,
                                'showInfo': False,
                                'message': 'The passwords do not match.'})
        except ValidationError:
            return jsonify({'success': False,
                            'showError': True,
                            'showInfo': False,
                            'message': 'Modification failed!'})
        return f(*args, **kw)
    return wrapper


def validate_registration_json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            validate(request.json, REGISTRATION_SCHEMA,
                     format_checker=FormatChecker())
            if request.json["password"] != request.json["password2"]:
                return jsonify({'success': False,
                                'showError': True,
                                'showInfo': False,
                                'message': 'The passwords do not match.'})
        except ValidationError, e:
            error_message = ''
            if len(e.path) == 0:
                error_message = 'Some of the required fields are empty.'
            elif e.path[0] == 'id':
                error_message = 'The chosen username is not correct e-mail.'
            else:
                error_message = 'The password is too short (minimum 5 chars).'
            return jsonify({'success': False,
                            'showError': True,
                            'showInfo': False,
                            'message': error_message})
        return f(*args, **kw)
    return wrapper


def validate_login_json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            validate(request.json, LOGIN_SCHEMA)
        except ValidationError:
            return jsonify({'success': False, 'message': 'Login failed!'})
        return f(*args, **kw)
    return wrapper
