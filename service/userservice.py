import uuid

from flask import jsonify

from flask_login import login_user
from repository.userrepository import UserRepository

user_repository = UserRepository()


class UserService:

    def authenticate_user(self, user_vm):
        id = user_vm["id"]
        password = user_vm["password"]
        user = user_repository.get_user_with_password(id, password)
        if user is not None:
            login_user(user)
            return jsonify({'success': True, 'message': 'OK'})
        return jsonify({'success': False, 'message': 'Login failed!'})

    def add_user(self, user_vm):
        if user_repository.get_user(user_vm["id"]) is not None:
            return jsonify({'success': False,
                            'showError': True,
                            'showInfo': False,
                            'message': 'The user already exists.'})
        else:
            user_vm["guid"] = str(uuid.uuid1())
            user_repository.add_user(user_vm)
            return jsonify({'success': True,
                            'showError': False,
                            'showInfo': True,
                            'message': 'Registration successful!',
                            'id': '',
                            'password': '',
                            'password2': ''})

    def modify_user(self, user_vm, current_user_id):
        if user_repository.get_user(current_user_id) is None:
            return jsonify({'success': False,
                            'showError': True,
                            'showInfo': False,
                            'message': 'Cannot find the specified user.'})
        else:
            user_repository.modify_user(user_vm, current_user_id)
            return jsonify({'success': True,
                            'showError': False,
                            'showInfo': True,
                            'message': 'Changes saved!',
                            'password': '',
                            'password2': ''})
