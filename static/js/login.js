var loginDataVm = null;
var registrationDataVm = null;

$( document ).ready( function() {
    initLoginVm();
    initRegistrationVm();
} );

function initLoginVm() {
    loginDataVm = {
        "id": ko.observable(),
        "password": ko.observable(),
        "success": ko.observable( true ),
        "message": ko.observable()
    };
    ko.applyBindings( loginDataVm, $('#loginArea')[0] );
}

function initRegistrationVm() {
    registrationDataVm = {
        "id": ko.observable(),
        "password": ko.observable(),
        "password2": ko.observable(),
        "success": ko.observable( true ),
        "message": ko.observable(),
        "showError": ko.observable( false ),
        "showInfo": ko.observable( false )
    }
    ko.applyBindings( registrationDataVm, $('#registrationArea')[0] );
}

function authenticate() {
    $.ajax( {
        url: "/authenticate",
        type: "POST",
        data: ko.toJSON( loginDataVm ),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function( result ) {
            if ( result.success ) {
                // TODO: make redirect endpoint configurable from Python
                document.location = "/"
            } else {
                ko.mapping.fromJS(result, {}, loginDataVm)
            }
        }
    } );
}

function register() {
    disableRegisterButton();
    $.ajax( {
        url: "/addUser",
        type: "POST",
        data: ko.toJSON( registrationDataVm ),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function( result ) {
            ko.mapping.fromJS(result, {}, registrationDataVm);
            enableRegisterButton();
        }
    } );
}

function enableRegisterButton() {
    $('#registerButton').prop('disabled', false);
}

function disableRegisterButton() {
    $('#registerButton').prop('disabled', true);
}

function showRegistrationModal() {
    registrationDataVm.id('');
    registrationDataVm.password('');
    registrationDataVm.password2('');
    registrationDataVm.showError(false);
    registrationDataVm.showInfo(false);
    registrationDataVm.message('');
    $('#registrationArea').modal('show');
}
