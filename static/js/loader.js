function startLoader() {
    let width = $( document ).width();
    $.blockUI( {
        css: {
            backgroundColor: 'transparent',
            border: 'none',
            left: width / 2 - 40
        },
        message: "<div class='loader'></div>"
    } );
}

function stopLoader() {
    setTimeout( $.unblockUI(), 500 );
}
