var budgetDataVm = null;

$( document ).ready( function() {
    initBudgetData();
} );

function initBudgetData() {
    startLoader();
    $.getJSON( "/getBudgetData", function( data ) {
        if ( data.state != null ) {
            if ( budgetDataVm == null ) {
                budgetDataVm = createBudgetDataVmFromJson( data );
            } else {
                updateBudgetDataVmFromJson( data, budgetDataVm );
            }
        }
        stopLoader();
    } );
}

function saveBudgetData() {
    startLoader();
    $.ajax( {
        url: "/saveBudgetData",
        type: "POST",
        data: jsonifyBudgetDataVm( budgetDataVm ),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function( data ) {
            updateBudgetDataVmFromJson( data, budgetDataVm );
            stopLoader();
        }
    } )
}

function computeSubTotal( amountRecords ) {
    let total = 0;
    if ( amountRecords != null ) {
        for ( let i = 0; i < amountRecords.length; i++ ) {
            if ( $.isNumeric( amountRecords[ i ].amount() ) ) {
                total += parseInt( amountRecords[ i ].amount() );
            }
        }
    }
    return total;
}

function computeTotal( flow, flowType ) {
    let total = 0;
    if ( flow != null ) {
        for ( let i = 0; i < flow.length; i++ ) {
            total += computeSubTotal( flow[ i ][ flowType ]() );
        }
    }
    return total;
}

function computeBalance( income, expenses, flowType ) {
    let totalIncome = computeTotal( income, flowType );
    let totalExpenses = computeTotal( expenses, flowType );
    return totalIncome - totalExpenses;
}

function addRecord( records ) {
    records.push( {
        guid: ko.observable( generateGuid() ),
        amount: ko.observable( "" ),
        description: ko.observable( "" )
    } );
}

function removeRecord( records, record ) {
    records.remove( record );
}

function addCategory( flow ) {
    let category = {
        "category": ko.observable( "New Category" ),
        "expected": ko.observableArray(),
        "actual": ko.observableArray()
    };
    addRecord( category.expected() );
    addRecord( category.actual() );
    flow.push( category );
}

function removeCategory( flow, category ) {
    flow.remove( category );
}

function addMonth( monthNr, budget ) {
    let month = {
        "month": ko.observable( monthNr ),
        "income": ko.observableArray(),
        "expenses": ko.observableArray()
    };
    addCategory( month.income() );
    addCategory( month.expenses() );
    budget.months().push( month );
}

function addBudget( year, currency, budgets ) {
    let budget = {
        "guid": ko.observable( generateGuid() ),
        "year": ko.observable( year ),
        "currency": ko.observable( currency ),
        "months": ko.observableArray()
    };
    for ( let i = 1; i <= 12; i++ ) {
        addMonth( i, budget );
    }
    budgets.push( budget );
    return budget.guid();
}

function removeBudget( budget ) {
    budgetDataVm.budgets.remove( budget );
    budgetDataVm.state.selectedBudget( null );
}

function updateBudgetDataVmFromJson( data, vm ) {
    // TODO: Do we need to add binding arguments here?
    ko.mapping.fromJS( data, vm );
    resetObserver( vm );
}

function createBudgetDataVmFromJson( data ) {
    let vm = ko.mapping.fromJS( data );
    activateBudgetDataVm( vm );
    return vm;
}

function activateBudgetDataVm( vm ) {
    vm.observer = new BudgetDataObserver( vm );
    ko.applyBindings( vm, $('#budgetArea')[0] );
}

function resetObserver( vm ) {
    vm.observer.initialState( ko.toJSON( vm.budgets ) );
}

function BudgetDataObserver( vm ) {
    self = this;
    self.initialState = ko.observable( ko.toJSON( vm.budgets ) );
    self.hasChanged = ko.dependentObservable( function() {
        return ko.toJSON( vm.budgets ) != self.initialState();
    } );
    self.reset = function() {
        self.initialState( ko.toJSON( vm.budgets ) );
    };
}

function generateGuid() {
    function s4() {
        return Math.floor( ( 1 + Math.random() ) * 0x10000 ).toString( 16 ).substring( 1 );
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function jsonifyBudgetDataVm( vm ) {
    let jsObj = ko.toJS( vm );
    let resultObj = {
        "state": jsObj.state,
        "budgets": jsObj.budgets
    };
    return JSON.stringify( resultObj );
}

function selectBudget( guid ) {
    budgetDataVm.state.selectedBudget( guid );
    budgetDataVm.state.selectedMonth( '1' );
    closeNav();
}

function openNewBudget() {
    let budgetGuid = addBudget( 'New Budget for <YEAR>', 'EUR', budgetDataVm.budgets );
    selectBudget( budgetGuid );
}

function selectMonth( month ) {
    budgetDataVm.state.selectedMonth( month );
}

function isSelectedBudget( vm, guid ) {
    return guid == vm.state.selectedBudget();
}
