var userVm = null;

$( document ).ready( function() {
    initUserVm();
} );

function initUserVm() {
    userVm = {
        "id": ko.observable(),
        "password": ko.observable(),
        "password2": ko.observable(),
        "success": ko.observable( true ),
        "message": ko.observable(),
        "showError": ko.observable( false ),
        "showInfo": ko.observable( false )
    }
    ko.applyBindings( userVm, $('#modificationArea')[0] );
}

function modifyUser() {
    disableUserModificationButton();
    $.ajax( {
        url: "/modifyUser",
        type: "POST",
        data: ko.toJSON( userVm ),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function( result ) {
            ko.mapping.fromJS(result, {}, userVm);
            enableUserModificationButton();
        }
    } );
}

function enableUserModificationButton() {
    $('#userModificationButton').prop('disabled', false);
}

function disableUserModificationButton() {
    $('#userModificationButton').prop('disabled', true);
}

function showUserModificationModal() {
    userVm.password('');
    userVm.password2('');
    userVm.showError(false);
    userVm.showInfo(false);
    userVm.message('');
    $('#modificationArea').modal({
        show: true
    });
}
