appdirs==1.4.3
click==6.7
Flask==0.12
Flask-Login==0.4.0
Flask-Testing==0.6.2
functools32==3.2.3.post2
itsdangerous==0.24
Jinja2==2.9.5
jsonschema==2.6.0
MarkupSafe==1.0
packaging==16.8
pyparsing==2.2.0
six==1.10.0
Werkzeug==0.12.1
