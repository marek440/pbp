
import os
import unittest
import json
from os import path

from flask import current_app as app
from flask import Flask
from werkzeug.security import check_password_hash

from flask_login import LoginManager, current_user
from flask_testing import TestCase
from repository.budgetrepository import BudgetRepository
from repository.userrepository import UserRepository
from service.userservice import UserService


class AppTestCase(TestCase):

    def test_add_user(self):
        result = self.create_user()
        status = result.json["success"]
        message = result.json["message"]
        self.assertIsNotNone(result)
        self.assertEqual(status, True)
        self.assertEqual(message, "Registration successful!")

        nok_id = "wrongUser@test.cm"
        ok_id = "test@test.com"
        nok_pass = "wrongPass"
        ok_pass = "test123"
        user = self.user_repository.get_user_with_password(nok_id, nok_pass)
        self.assertIsNone(user)
        user = self.user_repository.get_user_with_password(ok_id, nok_pass)
        self.assertIsNone(user)
        user = self.user_repository.get_user_with_password(nok_id, ok_pass)
        self.assertIsNone(user)
        user = self.user_repository.get_user_with_password(ok_id, ok_pass)
        self.assertIsNotNone(user)
        self.assertEqual(user.id, "test@test.com")
        self.assertFalse(check_password_hash(user.password, nok_pass))
        self.assertTrue(check_password_hash(user.password, ok_pass))

    def test_modify_user(self):
        self.create_user()
        self.modify_user()
        new_pass = "newPass"
        id = "test@test.com"
        user = self.user_repository.get_user_with_password(id, new_pass)
        self.assertIsNotNone(user)
        self.assertEqual(user.id, "test@test.com")

    def test_authenticate_user(self):
        self.assertIsNotNone(current_user)
        self.assertTrue(current_user.is_anonymous)
        self.assertFalse(current_user.is_authenticated)

        self.authenticate_user()
        self.assertIsNotNone(current_user)
        self.assertFalse(current_user.is_anonymous)
        self.assertTrue(current_user.is_authenticated)
        self.assertEqual(current_user.id, "test@test.com")
        self.assertIsNotNone(current_user.guid)

    def test_budget_operations(self):
        self.authenticate_user()
        budget = {
                "state": {
                    "selectedBudget": None,
                    "selectedMonth": 5
                },
                "budgets": []}
        self.budget_repository.write_budget_data(current_user, budget)
        budget_json = self.budget_repository.read_budget_data(current_user)
        budget = json.loads(budget_json)
        self.assertEqual(budget["state"]["selectedMonth"], 5)

        budget["state"]["selectedMonth"] = 11
        self.budget_repository.write_budget_data(current_user, budget)
        budget_json = self.budget_repository.read_budget_data(current_user)
        budget = json.loads(budget_json)
        self.assertEqual(budget["state"]["selectedMonth"], 11)

    def create_user(self):
        registration_json = {
            "id": "test@test.com",
            "password": "test123",
            "password2": "test123"
        }
        return self.user_service.add_user(registration_json)

    def modify_user(self):
        user_data_json = {
            "id": "test@test.com",
            "password": "newPass"
        }
        return self.user_service.modify_user(user_data_json, "test@test.com")

    def authenticate_user(self):
        self.create_user()
        login_json = {
            "id": "test@test.com",
            "password": "test123"
        }
        self.user_service.authenticate_user(login_json)

    def create_app(self):
        app = Flask(__name__)
        app.config["USERS_FILE_LOCATION"] = 'tests/db/user_test_data.json'
        app.config["BUDGETS_FOLDER"] = "tests/db/"
        app.config["SECRET_KEY"] = "93ad3669-6a43-4f14-a888-92f873802a6a"
        self.login_manager = LoginManager()
        self.login_manager.init_app(app)
        self.login_manager.login_view = "login"
        return app

    def setUp(self):
        self.user_service = UserService()
        self.user_repository = UserRepository()
        self.budget_repository = BudgetRepository()
        if not os.path.exists(app.config["BUDGETS_FOLDER"]):
            os.makedirs(app.config["BUDGETS_FOLDER"])

    def tearDown(self):
        if path.exists(app.config["USERS_FILE_LOCATION"]):
            os.remove(app.config["USERS_FILE_LOCATION"])
        for budget_file in os.listdir(app.config["BUDGETS_FOLDER"]):
            file_path = os.path.join(app.config["BUDGETS_FOLDER"], budget_file)
            os.unlink(file_path)


if __name__ == '__main__':
    unittest.main()
