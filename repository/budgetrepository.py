import json
import os

from flask import current_app as app


class BudgetRepository:

    def read_budget_data(self, user):
        if os.path.exists(self.get_path(user)):
            f = open(self.get_path(user), 'r')
            budgetJson = f.read()
            f.close()
            return budgetJson
        else:
            return '''{
                        "state": {
                            "selectedBudget": null,
                            "selectedMonth": "1"
                        },
                        "budgets": []}'''

    def write_budget_data(self, user, budget_data):
        f = file(self.get_path(user), "w")
        f.write(json.dumps(budget_data))
        f.close()

    def get_path(self, user):
        return app.config["BUDGETS_FOLDER"] + \
            'budgetData_' + user.guid + '.json'
