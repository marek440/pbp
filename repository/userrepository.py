import json
import os
import sys

from flask import current_app as app
from werkzeug.security import check_password_hash, generate_password_hash

from domain.user import User

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


class UserRepository:

    def get_user(self, id):
        user_data = self.get_users()
        for user in user_data["users"]:
            if user["id"] == id:
                return User(user)
        return None

    def get_user_with_password(self, id, password):
        if id is not None and password is not None:
            user = self.get_user(id)
            if user is not None:
                if check_password_hash(user.password, password):
                    return user
        return None

    def read_users_from_file(self):
        json_str = None
        if os.path.exists(app.config["USERS_FILE_LOCATION"]):
            f = open(app.config["USERS_FILE_LOCATION"], "r")
            json_str = f.read()
            f.close()
        if json_str is None:
            json_str = '{"users": []}'
        return json_str

    def get_users(self):
        json_str = self.read_users_from_file()
        return json.loads(json_str)

    def write_users_to_file(self, users):
        f = file(app.config["USERS_FILE_LOCATION"], "w")
        f.write(json.dumps(users))
        f.close()
        return None

    def add_user(self, user_vm):
        users = self.get_users()
        users["users"].append({
            "id": user_vm["id"],
            "password": generate_password_hash(user_vm["password"]),
            "guid": user_vm["guid"]
        })
        self.write_users_to_file(users)

    def modify_user(self, user_vm, current_user_id):
        users = self.get_users()
        for user in users["users"]:
            if user["id"] == current_user_id:
                user["password"] = generate_password_hash(user_vm["password"])
                self.write_users_to_file(users)
                return
